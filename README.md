# WikiCommonsData

Data available on [Commons.Wikimedia.org](https://commons.wikipedia.org) by @gorlapraveen is presented here. 

It may include Image(s), Text, Video(s) and Software Files.

---------------------------------------------------------------------------------------------------

## License

Content available including Image(s), Text, Video(s) and Software File(s) are licensed under [Creative Commons Attribution 4.0 International License](http://creativecommons.org/licenses/by/4.0/). Get copy of [License](/LICENSE)
